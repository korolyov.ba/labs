﻿using System;
using Xunit;
using Application;
using Moq;

namespace TankTests
{
    public class FireCommandTests
    {

        [Fact]
        public void FireTest()
        {


            bool isCreatBulletCalled = false;
            bool isSetPositionCalled = false;
            bool isActionCalled = false;
            bool isStartMovementCalled = false;


            bool isCreatBullet = true;
            bool isSetPosition = true;
            bool isAction = true;
            bool isStartMovement = true;


            var MockSetPositionCmd = new Mock<ICommand>();
            var MockBullet = new Mock<IUObject>();
            var MockAction = new Mock<IUObject>();
            var MockStartMovementCmd = new Mock<ICommand>();
            var MockFireableObject = new Mock<IFireable>();
            MockFireableObject.SetupGet(m => m.BulletPosition).Returns(new Vector(new int[] { 1, 1 })).Verifiable();
            MockFireableObject.SetupGet(m => m.BulletVelocity).Returns(new Vector(new int[] { 5, 5 })).Verifiable();
            var FireableObject = MockFireableObject.Object;



            IoC.Resolve<ICommand>("IoC.Register", "GameObjects.CreateBullet",(Func<object[],object>)((args) =>
            {
                isCreatBulletCalled = true;

                if (args.Length > 0)
                {
                    isCreatBullet = false;
                }

                return MockBullet.Object;

            })).Execute();

            IoC.Resolve<ICommand>("IoC.Register", "GameObjects.SetPosition", (Func<object[], object>)((args) =>
            {
                isSetPositionCalled = true;

                if 
                (
                    !(args.Length == 2) ||
                    !(args[0] is IUObject) ||
                    !(args[1] is Vector)
                )
                {
                    isSetPosition = false;
                }

                return MockSetPositionCmd.Object;

            })).Execute();

            IoC.Resolve<ICommand>("IoC.Register", "Action", (Func<object[], object>)((args) =>
            {
                isActionCalled = true;

                if 
                (
                    (args.Length != 3) || 
                    !(args[0] is IUObject) || 
                    !(args[1] is string) || 
                    !((string)args[1] == "Move") ||
                    !(args[2] is Vector)
                )
                {
                    isAction = false;
                }

                return MockAction.Object;

            })).Execute();

            IoC.Resolve<ICommand>("IoC.Register", "GameObjects.StartMovement", (Func<object[], object>)((args) =>
            {
                isStartMovementCalled = true;

                if 
                (
                    !(args.Length == 1) ||
                    !(args[0] is IUObject)
                )
                {
                    isStartMovement = false;
                }

                return MockStartMovementCmd.Object;

            })).Execute();



            new FireCommand(FireableObject).Execute();
            
            MockFireableObject.VerifyAll();



            Assert.True(isCreatBulletCalled);
            Assert.True(isCreatBullet);
            Assert.True(isSetPositionCalled);
            Assert.True(isSetPosition);
            Assert.True(isActionCalled);
            Assert.True(isAction);
            Assert.True(isStartMovementCalled);
            Assert.True(isStartMovement);

        }



    }
}
