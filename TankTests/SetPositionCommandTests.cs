﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Application;

namespace TankTests
{
    public class SetPositionCommandTests
    {



        [Fact]
        public void SetPositionCommandTest()
        {
            var InitialPosition = new Vector(new int[] { 2, 2});
            var MockObject = new Mock<IUObject>();
            MockObject.SetupSet(m => m["Position"] = It.Is<Vector>(v => v.Equals(new Vector(new int[]{ 2, 2 })))).Verifiable();
            new SetPositionCommand(MockObject.Object, InitialPosition).Execute();

            MockObject.VerifyAll();
        }



    }
}
