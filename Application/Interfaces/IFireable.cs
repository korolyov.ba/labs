﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public interface IFireable
    {
        Vector BulletPosition
        {
            get;
        }

        Vector BulletVelocity
        {
            get;
        }

    }
}
