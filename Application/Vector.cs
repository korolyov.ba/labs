﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class Vector
    {
        int[] value;



        public int Size
        {
            get => value.Length;
        }



        public Vector(int[] value)
        {
            this.value = value;
        }



        public static Vector operator +(Vector vector1, Vector vector2)
        {
            int[] vector = new int[vector1.Size];
            for (int i = 0; i < vector1.Size; i++)
            {
                vector[i] = vector1.value[i] + vector2.value[i];
            }
            return new Vector(vector);
        }



        public static implicit operator Vector(int v)
        {
            throw new NotImplementedException();
        }



        public override bool Equals(object o1)
        {
            if (o1 is Vector)
            {
                if (value.Length != ((Vector)o1).value.Length)
                    return false;
                for (int i = 0; i < Size; i++)
                {
                    if (value[i] != ((Vector)o1).value[i])
                        return false;
                }
                return true;
            }
            else
                return false;
        }
    }
}
