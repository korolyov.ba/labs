﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Application
{


    public class IoC
    {
        class IoCRegisterCommand : ICommand
        {
            private string key;
            private Func<object[], object> strategy;

            public IoCRegisterCommand(string key, Func<object[], object> strategy)
            {
                this.key = key;
                this.strategy = strategy;
            }

            public void Execute()
            {
                dependencies.Add(key, strategy);
            }
        }



        class IoCRemoveDependencyCommand : ICommand
        {
            string key;

            public IoCRemoveDependencyCommand(string key)
            {
                this.key = key;
            }

            public void Execute()
            {
                dependencies.Remove(key);
            }
        }



        static Dictionary<string, Func<object[], object>> dependencies = new Dictionary<string, Func<object[], object>>()
        {
            {
                "IoC.Register",
                (args) => { return new IoCRegisterCommand((string)args[0], (Func<object[], object>)args[1]); }
            },
            {
                "IoC.RemoveDependency",
                (args) => { return new IoCRemoveDependencyCommand((string)args[0]); }
            }
        };



        public static T Resolve<T>(string key, params object[] args)
        {
            try
            {
                return (T)dependencies[key](args);
            }
            catch (KeyNotFoundException)
            {
                throw new IoCException("KeyNotFound");
            }
            catch (InvalidCastException)
            {
                throw new IoCException("WrongArguments");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
