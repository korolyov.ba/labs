﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class SetPositionCommand : ICommand
    {
        IUObject Object;
        Vector InitialPosition; 

        public SetPositionCommand(IUObject Object, Vector InitialPosition)
        {
            this.Object = Object;
            this.InitialPosition = InitialPosition;
        }

        public void Execute()
        {
            Object["Position"] = InitialPosition;
        }
    }
}
