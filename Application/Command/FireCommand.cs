﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class FireCommand : ICommand
    {

        IFireable fireable;

        public FireCommand(IFireable fireable)
        {
            this.fireable = fireable;
        }

        public void Execute()
        {
            var bullet = IoC.Resolve<IUObject>("GameObjects.CreateBullet");
            IoC.Resolve<ICommand>("GameObjects.SetPosition", bullet, fireable.BulletPosition).Execute();
            var action = IoC.Resolve<IUObject>("Action", bullet, "Move", fireable.BulletVelocity);
            IoC.Resolve<ICommand>("GameObjects.StartMovement", action).Execute();
        }

    }
}
