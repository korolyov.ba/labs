﻿//TODO Написать тест для StartRotate

using System;
using Moq;
using Xunit;
using ConsoleApplication1;
using System.Numerics;
using Newtonsoft.Json;


namespace TestProject1
{
    public class Tests
    {
        [Fact]
        public void Rotate()
        {
            //Arrange
            var mock = new Mock<IRotatable>();

            mock.SetupGet(m => m.Direction).Returns(new Vector2(2, 5)).Verifiable();
            mock.SetupGet(m => m.Velocity).Returns(45).Verifiable();
            mock.SetupSet(m => m.Direction = It.IsAny<Vector2>()).Verifiable();

            RotateCommand c = new RotateCommand(mock.Object);
            //Action
            c.Execute();
            //Assert
            mock.VerifyAll();
        }


        [Fact]
        public void StopRotate()
        {
            var m = new Mock<IRotateStoppable>();
            var obj = m.Object;
            var Empty = new Mock<ICommand>();
            StopRotate c = new StopRotate(obj);
            bool isEmptyCalled = true;
            bool isEmptyCommand = true;
            IoC.Resolve<ICommand>("IoC.Register", "EmptyCommand", (Func<object[], object>) ((args) =>
            {
                isEmptyCalled = true;

                if (args.Length != 1 || !(args[0] is IRotateStoppable))
                {
                    isEmptyCommand = false;
                }

                return Empty.Object;
            })).Execute();

            Assert.True(isEmptyCommand);
            Assert.True(isEmptyCalled);
        }


        [Fact]
        public void StartRotate()
        {
        }
    }
}