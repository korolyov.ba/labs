﻿using System;
using Moq;
using Xunit;
using ConsoleApplication1;
using System.Numerics;
using Newtonsoft.Json;

namespace TestProject1
{
    public class IoCTests
    {
        public class TestAcceptException : Exception
        {
            public TestAcceptException(string message)
                : base(message)
            {
            }
        }


        [Fact]
        public void IoCRegisterTest()
        {
            Func<object[], object> action1 = (args) => { return (int) args[0] + (int) args[1]; };

            Func<object[], object> action2 = (args) => { throw new TestAcceptException("Success"); };


            IoC.Resolve<ICommand>("IoC.Register", "Action1", action1).Execute();
            IoC.Resolve<ICommand>("IoC.Register", "Action2", action2).Execute();


            Assert.Equal(4, IoC.Resolve<int>("Action1", 2, 2));
            Assert.Throws<TestAcceptException>
            (() => { IoC.Resolve<object>("Action2"); }
            );
            Assert.Throws<IoCException>
            (() => { IoC.Resolve<object>("Action3"); }
            );
            Assert.Throws<IoCException>
            (() => { IoC.Resolve<object>("Action1", 2, new Exception()); }
            );
        }


        [Fact]
        public void IoCRemoveDependencyTest()
        {
            Func<object[], object> action = (args) => { throw new TestAcceptException("Success"); };

            IoC.Resolve<ICommand>("IoC.Register", "Action", action).Execute();

            Assert.Throws<TestAcceptException>
            (() => { IoC.Resolve<object>("Action"); }
            );

            IoC.Resolve<ICommand>("IoC.RemoveDependency", "Action").Execute();

            Assert.Throws<IoCException>
            (() => { IoC.Resolve<object>("Action"); }
            );
        }

    }
}