﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class IoCException : Exception
    {
        public IoCException(string message)
            : base(message) { }
    }

    public class IoCExceptionUnExpected : Exception
    {
        public IoCExceptionUnExpected(string message)
            : base(message) { }
    }
}