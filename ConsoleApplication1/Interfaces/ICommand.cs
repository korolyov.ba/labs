﻿namespace ConsoleApplication1
{
    public interface ICommand
    {
        void Execute();
    }
}