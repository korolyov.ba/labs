﻿namespace ConsoleApplication1
{
    public interface IRotateStoppable
    {
        Injector Stop();
    }
}