﻿namespace ConsoleApplication1
{
    public interface IStartRotatable
    {
        IUObject GetSource();

        double RotateVelocity { get; set; }

        ICommand SetInjector { get; set; }
    }
}