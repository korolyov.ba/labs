﻿using System.Numerics;

namespace ConsoleApplication1
{
    public interface IRotatable
    {
        Vector2 Direction { get; set; }
        double Velocity { get; set; }
    }
}