﻿namespace ConsoleApplication1
{
    public class RotateMacroCommand : ICommand
        {
            private IRotatable Rotatable;

            public RotateMacroCommand(IRotatable Rotatable)
            {
                this.Rotatable = Rotatable;
            }
            public void Execute()
            {
                new RotateCommand(Rotatable).Execute();

                ICommand Injector = IoC.Resolve<ICommand>("Injector", Rotatable);
                ICommand cmd = IoC.Resolve<ICommand>("PushCommand", Injector);

                cmd.Execute();
            }
        }
    
}