﻿namespace ConsoleApplication1
{
    public class StartRotate : ICommand
    {
        private IStartRotatable StartRotatable;
        private double Velocity;
        public StartRotate(IStartRotatable StartRotatable, double Velocity)
        {
            this.StartRotatable = StartRotatable;
            this.Velocity = Velocity;
        }
        public void Execute()
        {
            ICommand RotateCommand = IoC.Resolve<ICommand>("Rotate", StartRotatable);
            ICommand Injector = IoC.Resolve<ICommand>("Injector", RotateCommand);
            StartRotatable.SetInjector = Injector;
            ICommand cmd = IoC.Resolve<ICommand>("PushCommand", Injector);
            cmd.Execute();

        }
    }
}