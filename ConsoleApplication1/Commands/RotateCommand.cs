﻿using System;
using System.Numerics;

namespace ConsoleApplication1
{
    public class RotateCommand : ICommand
    {
        private IRotatable Rotatable;

        public RotateCommand(IRotatable Rotatable)
        {
            this.Rotatable = Rotatable;
        }
        public void Execute()
        {
            Rotatable.Direction = new Vector2(
                (float) Math.Round(Rotatable.Direction.X * Math.Cos((Math.PI / 180) * Rotatable.Velocity))
                - (float) Math.Round(Rotatable.Direction.Y * Math.Sin((Math.PI / 180) * Rotatable.Velocity)),
                (float) Math.Round(Rotatable.Direction.Y * Math.Cos((Math.PI / 180) * Rotatable.Velocity))
                + (float) Math.Round(Rotatable.Direction.X * Math.Sin((Math.PI / 180) * Rotatable.Velocity))
            );
            //x' = xcos - ysin;
            //y' = ycos + xsin;
        }
    }
}