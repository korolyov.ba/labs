﻿using System.Linq;

namespace ConsoleApplication1
{
    public class StopRotate : ICommand
    {
        IRotateStoppable StopRotatable;

        public StopRotate(IRotateStoppable stopRotatable)
        {
            this.StopRotatable = stopRotatable;
        }

        public void Execute()
        {
            Injector RotateCommand = StopRotatable.Stop();
            IoC.Resolve<ICommand>("MakeCommandEmpty", RotateCommand);

        }
    }
}