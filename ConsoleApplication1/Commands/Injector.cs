﻿namespace ConsoleApplication1
{
    public class Injector : ICommand 
    {
        private ICommand command;
        public Injector(ICommand command)
        {
            this.command = command;
        }
        public void setCommand(ICommand cmd)
        {
            this.command = cmd;
        }
        public void Execute()
        {
            command.Execute();
        }

    }
}